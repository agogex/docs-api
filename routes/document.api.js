const router      = require('express').Router();
const redis       = require('redis');
const config      = require('../config');
const Document    = require('../models/document.model');
const redisClient = redis.createClient(config.redisPort);

redisClient.on('error', err => console.error(err));

router.get('/documents',
    (req, res, next) => {
        redisClient.get('docs', (err, docs) => {
            if (err) next({ status: 404, message: 'User not found.' });
            if (docs !== null) {
                res.status(200).json(JSON.parse(docs));
            } else {
                next();
            }
        });
    },
    (req, res, next) => {
        Document.find({})
            .then(docs => {
                redisClient.setex('docs', config.cacheTime, JSON.stringify(docs));
                res.status(200).json(docs);
            })
            .catch(err => next({
                status: 404,
                message: 'User not found.'
            }));
    });

router.get('/documents/:id',
    (req, res, next) => {
        let name = req.params.id;
        redisClient.get(name, (err, docs) => {
            if (err) next({
                status: 404,
                message: 'User not found.'
            });
            if (docs !== null) {
                res.status(200).json(JSON.parse(docs));
            }
            else {
                next();
            }
        });
    },
    (req, res, next) => {
        const _id = req.params.id.split(',');
        Document.find({
            _id
        })
            .then(doc => {
                let name = req.params.id;
                redisClient.setex(name, config.cacheTime, JSON.stringify(doc));
                res.status(200).json(doc);
            })
            .catch(err => next({
                status: 404,
                message: 'User not found.'
            }));
    });

router.post('/documents', (req, res, next) => {
    if (Object.keys(req.body).length === 0) next({ status: 400, message: 'Bad Request.' });
    const doc = new Document(req.body);
    doc.save()
        .then(doc => res.status(201).json(doc))
        .catch(err => next({ status: 400, message: 'Bad Request.' }));
});

module.exports = router;