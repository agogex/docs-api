const express     = require('express');
const mongoose    = require('mongoose');
const bodyParser  = require('body-parser');
const docsRouter  = require('./routes/document.api');
const config      = require('./config');
const app         = express();
const mongooseUrl = `mongodb://${config.dbUser}:${config.dbPass}@${config.dbHost}/${config.dbName}`;
mongoose.Promise  = global.Promise;

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use('/api', docsRouter);

app.use((err, req, res, next) => {
    let status  = err.status || 500;
    let message = err.message || 'Internal Server Error';
    res.status(status).json({
        message
    });
});

app.listen(config.port, () => {
    mongoose.connect(
        mongooseUrl,
        (err) => {
            if (err) console.error(err);
        }
    );
    console.log(`Server is running on ${config.port}`);
});