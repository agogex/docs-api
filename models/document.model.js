const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const documentSchema = Schema({
    title  : String,
    content: String
});

module.exports = mongoose.model('Document', documentSchema);