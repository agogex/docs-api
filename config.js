module.exports = {
    port     : process.env.PORT || 3000,
    host     : process.env.HOST,
    dbUser   : "docs-api-user",
    dbPass   : "docs-api-password",
    dbHost   : "ds131512.mlab.com:31512",
    dbName   : "docs-api",
    redisPort: process.env.REDIS_PORT || 6379,
    cacheTime: 360
}